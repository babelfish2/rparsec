#![allow(unused_variables)]
#![warn(clippy::all)]

extern crate lexical;
extern crate unic;
#[cfg(feature = "serde")]
#[macro_use]
extern crate serde;

pub mod bytes;
pub mod combinator;
pub mod error;
pub mod input;
pub mod misc;
pub mod numeric;
pub mod state;
pub mod text;

pub type ParseResult<I, O, S> = Result<(I, O, S), (I, error::ParseError, S)>;
// pub trait Parser<I, O, S> = Fn(I, S) -> ParseResult<I, O, S>; // seriously, I want trait aliases in stable so badly
