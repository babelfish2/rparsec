use super::error::{
    ParseError,
    ParseErrorKind::{InfiniteLoop, InvalidParser},
};
use super::input::ParserInput;
use super::state::ParserState;
use super::ParseResult;

/// Attempts to apply `p1` followed by `p2` and returns a tuple containing the output of both if successful.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::pair;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
/// use rparsec::error::{ParseError, ParseErrorKind::InputMismatch};
///
/// let parser = pair(character('a'), character('b'));
/// let successful_input = "abc";
/// let failing_input = "acc";
/// let state = NullState {};
///
/// assert_eq!(parser(successful_input, state), Ok(("c", ('a', 'b'), state)));
/// assert_eq!(parser(failing_input, state), Err(("acc", ParseError(InputMismatch(String::from("Expected character \'b\', found \'c\'.")), None), state)));
/// ```
pub fn pair<I, O1, O2, S, F1, F2>(
    p1: F1,
    p2: F2,
) -> impl Fn(I, S) -> ParseResult<I, (O1, O2), S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O1: Clone,
    O2: Clone,
    F1: Fn(I, S) -> ParseResult<I, O1, S> + Clone,
    F2: Fn(I, S) -> ParseResult<I, O2, S> + Clone,
{
    move |input: I, state: S| {
        let original_input = input.clone();
        let original_state = state.clone();

        p1(input, state).and_then(|(input, o1, state)| {
            p2(input, state)
                .map(|(input, o2, state)| (input, (o1, o2), state))
                .map_err(|(_, reason, _)| (original_input, reason, original_state))
        })
    }
}

/// Attempts to apply `preceder` followed by `parser` and returns the output of `parser` if both are successful.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::preceded;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
/// use rparsec::error::{ParseError, ParseErrorKind::InputMismatch};
///
/// let parser = preceded(character('a'), character('b'));
/// let successful_input = "abc";
/// let failing_input = "acc";
/// let state = NullState {};
///
/// assert_eq!(parser(successful_input, state), Ok(("c", 'b', state)));
/// assert_eq!(parser(failing_input, state), Err(("acc", ParseError(InputMismatch(String::from("Expected character \'b\', found \'c\'.")), None), state)));
/// ```
pub fn preceded<I, O1, O2, S, F1, F2>(
    preceder: F1,
    parser: F2,
) -> impl Fn(I, S) -> ParseResult<I, O2, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O1: Clone,
    O2: Clone,
    F1: Fn(I, S) -> ParseResult<I, O1, S> + Clone,
    F2: Fn(I, S) -> ParseResult<I, O2, S> + Clone,
{
    move |input: I, state: S| {
        let original_input = input.clone();
        let original_state = state.clone();

        let (input, _, state) = preceder(input, state)
            .map_err(|(_, reason, _)| (original_input.clone(), reason, original_state.clone()))?;

        parser(input, state).map_err(|(_, reason, _)| (original_input, reason, original_state))
    }
}

/// Attempts to apply `parser` followed by `terminator` and returns the output of `parser` if both are successful.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::terminated;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
/// use rparsec::error::{ParseError, ParseErrorKind::InputMismatch};
///
/// let parser = terminated(character('a'), character('b'));
/// let successful_input = "abc";
/// let failing_input = "acc";
/// let state = NullState {};
///
/// assert_eq!(parser(successful_input, state), Ok(("c", 'a', state)));
/// assert_eq!(parser(failing_input, state), Err(("acc", ParseError(InputMismatch(String::from("Expected character \'b\', found \'c\'.")), None), state)));
/// ```
pub fn terminated<I, O1, O2, S, F1, F2>(
    parser: F1,
    terminator: F2,
) -> impl Fn(I, S) -> ParseResult<I, O1, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O1: Clone,
    O2: Clone,
    F1: Fn(I, S) -> ParseResult<I, O1, S> + Clone,
    F2: Fn(I, S) -> ParseResult<I, O2, S> + Clone,
{
    move |input: I, state: S| {
        let original_input = input.clone();
        let original_state = state.clone();

        let (input, output, state) = parser(input, state)
            .map_err(|(_, reason, _)| (original_input.clone(), reason, original_state.clone()))?;
        let (input, _, state) = terminator(input, state)
            .map_err(|(_, reason, _)| (original_input, reason, original_state))?;

        Ok((input, output, state))
    }
}

/// Accepts an array slice of parsers and attempts to apply them in order until one succeeds. Returns the value of the succeeding parser.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::choice;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
///
/// let parser_choices = [character('a'), character('b')];
/// let parser = choice(&parser_choices);
/// let input = "bcd";
/// let state = NullState {};
///
/// assert_eq!(parser(input, state), Ok(("cd", 'b', state)));
/// ```
pub fn choice<'a, I, O, S, F>(list: &'a [F]) -> impl Fn(I, S) -> ParseResult<I, O, S> + Clone + 'a
where
    I: ParserInput,
    S: ParserState,
    O: Clone,
    F: Fn(I, S) -> ParseResult<I, O, S> + Clone,
{
    move |input: I, state: S| {
        let mut failure = None;

        for f in list {
            let result = f(input.clone(), state.clone());

            match result {
                Ok(_) => return result,
                Err((_, reason, _)) => match failure {
                    Some(prev_failure) => failure = Some(reason.set_cause(prev_failure)),
                    None => failure = failure,
                },
            }
        }

        Err((input, failure.unwrap(), state))
    }
}

/// Matches `n` occurrences of the provided parser. `n` must be a positive integer. If `n` is zero this will return a successful result containing an empty `Vec<O>`.
///
/// # Errors
///
/// An error will be returned if `n` occurrences of the provided parser cannot be successfully parsed.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::match_n;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
///
/// let parser = match_n(3, character('a'));
/// let input = "aaaab";
/// let state = NullState {};
///
/// assert_eq!(parser(input, state), Ok(("ab", vec!['a', 'a', 'a'], state)));
///
/// let parser = match_n(0, character('a'));
///
/// assert_eq!(parser(input, state), Ok(("aaaab", vec![], state)));
/// ```
pub fn match_n<I, O, S, F>(
    n: usize,
    parser: F,
) -> impl Fn(I, S) -> ParseResult<I, Vec<O>, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O: Clone,
    F: Fn(I, S) -> ParseResult<I, O, S> + Clone,
{
    move |input: I, state: S| {
        if n == 0 {
            return Ok((input, vec![], state));
        }

        let mut input = input.clone();
        let mut state = state.clone();
        let mut acc = Vec::with_capacity(n);

        for _ in 1..=n {
            match parser(input, state) {
                Ok((i, output, s)) => {
                    acc.push(output);
                    input = i;
                    state = s;
                }
                Err((i, reason, s)) => return Err((i, reason, s)),
            }
        }

        Ok((input, acc, state))
    }
}

/// `match_m_n` matches a minimum of `m` and a maximum of `n` occurrences of the provided parser. `m` and `n` must be a positive integers. If `n` is zero this will return a successful result containing an empty `Vec<O>`.
///
/// # Errors
///
/// An error will be returned if at least `m` occurrences of the provided parser cannot be successfully parsed, if `m` or `n` cannot be cast to a `usize`, or if `m` is greater than `n`.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::match_m_n;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
///
/// let parser = match_m_n(2, 3, character('a'));
/// let successful_input1 = "aab";
/// let successful_input2 = "aaab";
/// let failing_input = "ab";
/// let state = NullState {};
///
/// assert_eq!(parser(successful_input1, state), Ok(("b", vec!['a', 'a'], state)));
/// assert_eq!(parser(successful_input2, state), Ok(("b", vec!['a', 'a', 'a'], state)));
/// assert!(parser(failing_input, state).is_err());
///
/// let parser = match_m_n(3, 2, character('a'));
/// assert!(parser(successful_input1, state).is_err());
/// assert!(parser(successful_input2, state).is_err());
/// ```
pub fn match_m_n<I, O, S, F>(
    m: usize,
    n: usize,
    parser: F,
) -> impl Fn(I, S) -> ParseResult<I, Vec<O>, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O: Clone,
    F: Fn(I, S) -> ParseResult<I, O, S> + Clone,
{
    move |input: I, state: S| {
        if m > n {
            let err_msg = String::from(
                "Incorrect usage of rparsec::combinator::match_m_n, m cannot be greater than n.",
            );

            return Err((input, ParseError(InvalidParser(err_msg), None), state));
        }

        if n == 0 {
            return Ok((input, vec![], state));
        }

        let mut input = input.clone();
        let mut state = state.clone();
        let mut acc = Vec::with_capacity(n);
        let mut matches: usize = 0;

        for _ in 1..=n {
            match parser(input, state) {
                Ok((i, output, s)) => {
                    acc.push(output);
                    matches += 1;
                    input = i;
                    state = s;
                }
                Err((i, reason, s)) => {
                    if matches >= m {
                        return Ok((i, acc, s));
                    } else {
                        return Err((i, reason, s));
                    }
                }
            }
        }

        Ok((input, acc, state))
    }
}

/// Attempts to apply `open`, then `parser`, then `close`. If all three are successful, returns the output of `parser`.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::between;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
/// use rparsec::error::{ParseError, ParseErrorKind::InputMismatch};
///
/// let parser = between(character('('), character('a'), character(')'));
/// let successful_input = "(a)b";
/// let failing_input = "(ab";
/// let state = NullState {};
///
/// assert_eq!(parser(successful_input, state), Ok(("b", 'a', state)));
/// assert_eq!(parser(failing_input, state), Err(("(ab", ParseError(InputMismatch(String::from("Expected character \')\', found \'b\'.")), None), state)));
/// ```
pub fn between<I, O1, O2, O3, S, F1, F2, F3>(
    open: F1,
    parser: F2,
    close: F3,
) -> impl Fn(I, S) -> ParseResult<I, O2, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O1: Clone,
    O2: Clone,
    O3: Clone,
    F1: Fn(I, S) -> ParseResult<I, O1, S> + Clone,
    F2: Fn(I, S) -> ParseResult<I, O2, S> + Clone,
    F3: Fn(I, S) -> ParseResult<I, O3, S> + Clone,
{
    move |input: I, state: S| {
        open(input.clone(), state.clone())
            .and_then(|(input, _, state)| parser(input, state))
            .and_then(|(input, output, state)| close(input, state).map(|result| (result, output)))
            .map(|((input, _, state), output)| (input, output, state))
            .or_else(|(_, reason, _)| Err((input, reason, state)))
    }
}

/// Attempts to apply `parser` and returns an Option<O> containing the output or None.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::option;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
///
/// let parser = option(character('a'));
/// let successful_input = "ab";
/// let failing_input = "bb";
/// let state = NullState {};
///
/// assert_eq!(parser(successful_input, state), Ok(("b", Some('a'), state)));
/// assert_eq!(parser(failing_input, state), Ok(("bb", None, state)));
/// ```
pub fn option<I, O, S, F>(parser: F) -> impl Fn(I, S) -> ParseResult<I, Option<O>, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O: Clone,
    F: Fn(I, S) -> ParseResult<I, O, S> + Clone,
{
    move |input: I, state: S| {
        parser(input.clone(), state.clone())
            .map(|(i, o, s)| (i, Some(o), s))
            .or_else(|_| Ok((input, None, state)))
    }
}

/// Attempts to apply `parser` and returns the output if successful, otherwise it returns `default`.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::option_or_default;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
///
/// let parser = option_or_default(character('a'), 'a');
/// let successful_input = "ab";
/// let failing_input = "bb";
/// let state = NullState {};
///
/// assert_eq!(parser(successful_input, state), Ok(("b", 'a', state)));
/// assert_eq!(parser(failing_input, state), Ok(("bb", 'a', state)));
/// ```
pub fn option_or_default<I, O, S, F>(
    parser: F,
    default: O,
) -> impl Fn(I, S) -> ParseResult<I, O, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O: Clone,
    F: Fn(I, S) -> ParseResult<I, O, S> + Clone,
{
    move |input: I, state: S| {
        parser(input.clone(), state.clone()).or_else(|_| Ok((input, default.clone(), state)))
    }
}

/// Consumes zero or more instances of `parser` and discards the results.
///
/// # Errors
///
/// If the parser is applied and returns a successful result, but does not consume any of the input, this will return an error in order to prevent an infinite loop.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::skip_many0;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
///
/// let parser = skip_many0(character('a'));
/// let input1 = "b";
/// let input2 = "aaaaaab";
/// let state = NullState {};
///
/// assert_eq!(parser(input1, state), Ok(("b", (), state)));
/// assert_eq!(parser(input2, state), Ok(("b", (), state)));
/// ```
pub fn skip_many0<I, O, S, F>(parser: F) -> impl Fn(I, S) -> ParseResult<I, (), S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O: Clone,
    F: Fn(I, S) -> ParseResult<I, O, S> + Clone,
{
    move |input: I, state: S| {
        let original_input = input.clone();
        let original_state = state.clone();
        let mut prev_input: I;
        let mut input = input;
        let mut state = state;

        loop {
            prev_input = input.clone();

            match parser(input, state) {
                Ok((i, output, s)) => {
                    if i == prev_input {
                        let err_msg = String::from("skip_many0 successfully applied its parser, but failed to consume any input.");

                        return Err((
                            original_input,
                            ParseError(InfiniteLoop(err_msg), None),
                            original_state,
                        ));
                    } else {
                        input = i;
                        state = s;
                    }
                }
                Err((i, _, s)) => return Ok((i, (), s)),
            }
        }
    }
}

/// Consumes one or more occurrences of `parser` and discards the results.
///
/// # Errors
///
/// If the parser is applied and returns a successful result, but does not consume any of the input, this will return an error in order to prevent an infinite loop.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::skip_many1;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
/// use rparsec::error::{ParseError, ParseErrorKind::InputMismatch};
///
/// let parser = skip_many1(character('a'));
/// let successful_input = "aaaaaab";
/// let failing_input = "b";
/// let state = NullState {};
///
/// assert_eq!(parser(successful_input, state), Ok(("b", (), state)));
/// assert_eq!(parser(failing_input, state), Err(("b", ParseError(InputMismatch(String::from("Expected character \'a\', found \'b\'.")), None), state)));
/// ```
pub fn skip_many1<I, O, S, F>(parser: F) -> impl Fn(I, S) -> ParseResult<I, (), S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O: Clone,
    F: Fn(I, S) -> ParseResult<I, O, S> + Clone,
{
    move |input: I, state: S| {
        let original_input = input.clone();
        let original_state = state.clone();
        let mut prev_input: I;
        let mut input = input;
        let mut state = state;
        let mut matched_any = false;

        loop {
            prev_input = input.clone();

            match parser(input, state) {
                Ok((i, output, s)) => {
                    if i == prev_input {
                        let err_msg = String::from("skip_many1 successfully applied its parser, but failed to consume any input.");

                        return Err((
                            original_input,
                            ParseError(InfiniteLoop(err_msg), None),
                            original_state,
                        ));
                    } else {
                        matched_any = true;
                        input = i;
                        state = s;
                    }
                }
                Err((i, reason, s)) => {
                    if matched_any {
                        return Ok((i, (), s));
                    } else {
                        return Err((i, reason, s));
                    }
                }
            }
        }
    }
}

/// Applies `parser` zero or more times and returns a vector of the outputs of `parser`.
///
/// # Errors
///
/// If the parser is applied and returns a successful result, but does not consume any of the input, this will return an error in order to prevent an infinite loop.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::many0;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
///
/// let parser = many0(character('a'));
/// let input1 = "b";
/// let input2 = "aaaaaab";
/// let state = NullState {};
///
/// assert_eq!(parser(input1, state), Ok(("b", vec![], state)));
/// assert_eq!(parser(input2, state), Ok(("b", vec!['a', 'a', 'a', 'a', 'a', 'a'], state)));
/// ```
pub fn many0<I, O, S, F>(parser: F) -> impl Fn(I, S) -> ParseResult<I, Vec<O>, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O: Clone,
    F: Fn(I, S) -> ParseResult<I, O, S> + Clone,
{
    move |input: I, state: S| {
        let original_input = input.clone();
        let original_state = state.clone();
        let mut prev_input: I;
        let mut input = input;
        let mut state = state;
        let mut acc = Vec::new();

        loop {
            prev_input = input.clone();

            match parser(input, state) {
                Ok((i, output, s)) => {
                    if i == prev_input {
                        let err_msg = String::from("many0 successfully applied its parser, but failed to consume any input.");

                        return Err((
                            original_input,
                            ParseError(InfiniteLoop(err_msg), None),
                            original_state,
                        ));
                    } else {
                        acc.push(output);
                        input = i;
                        state = s;
                    }
                }
                Err((i, reason, s)) => return Ok((i, acc, s)),
            }
        }
    }
}

/// Applies `parser` one or more times and returns a list of the outputs of `parser`.
///
/// # Errors
///
/// If the parser is applied and returns a successful result, but does not consume any of the input, this will return an error in order to prevent an infinite loop.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::many1;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
/// use rparsec::error::{ParseError, ParseErrorKind::InputMismatch};
///
/// let parser = many1(character('a'));
/// let successful_input = "aaaaaab";
/// let failing_input = "b";
/// let state = NullState {};
///
/// assert_eq!(parser(successful_input, state), Ok(("b", vec!['a', 'a', 'a', 'a', 'a', 'a'], state)));
/// assert_eq!(parser(failing_input, state), Err(("b", ParseError(InputMismatch(String::from("Expected character \'a\', found \'b\'.")), None), state)));
/// ```
pub fn many1<I, O, S, F>(parser: F) -> impl Fn(I, S) -> ParseResult<I, Vec<O>, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O: Clone,
    F: Fn(I, S) -> ParseResult<I, O, S> + Clone,
{
    move |input: I, state: S| {
        let original_input = input.clone();
        let original_state = state.clone();
        let mut prev_input: I;
        let mut input = input;
        let mut state = state;
        let mut acc = Vec::new();

        loop {
            prev_input = input.clone();

            match parser(input, state) {
                Ok((i, output, s)) => {
                    if i == prev_input {
                        let err_msg = String::from("many1 successfully applied its parser, but failed to consume any input.");

                        return Err((
                            original_input,
                            ParseError(InfiniteLoop(err_msg), None),
                            original_state,
                        ));
                    } else {
                        acc.push(output);
                        input = i;
                        state = s;
                    }
                }
                Err((i, reason, s)) => {
                    if !acc.is_empty() {
                        return Ok((i, acc, s));
                    } else {
                        return Err((i, reason, s));
                    }
                }
            }
        }
    }
}

/// Applies `end`, if `end` is unsuccessful then it tries to apply `parser` followed by `end` until `parser` fails or `end` succeeds. If this is successful, it returns a tuple with the list of the outputs of `parser` and the output of `end`.
///
/// # Errors
///
/// If `parser` is applied and returns a successful result, but does not consume any of the input, this will return an error in order to prevent an infinite loop.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::many_till0;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
/// use rparsec::error::{ParseError, ParseErrorKind::InputMismatch};
///
/// let parser = many_till0(character('a'), character('b'));
/// let successful_input1 = "aaaaaabb";
/// let successful_input2 = "bb";
/// let failing_input = "aaaaaacc";
/// let state = NullState {};
///
/// assert_eq!(parser(successful_input1, state), Ok(("b", (vec!['a', 'a', 'a', 'a', 'a', 'a'], 'b'), state)));
/// assert_eq!(parser(successful_input2, state), Ok(("b", (vec![], 'b'), state)));
/// assert_eq!(parser(failing_input, state), Err(("aaaaaacc", ParseError(InputMismatch(String::from("Expected character \'a\', found \'c\'.")), None), state)));
/// ```
pub fn many_till0<I, O1, O2, S, F1, F2>(
    parser: F1,
    end: F2,
) -> impl Fn(I, S) -> ParseResult<I, (Vec<O1>, O2), S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O1: Clone,
    O2: Clone,
    F1: Fn(I, S) -> ParseResult<I, O1, S> + Clone,
    F2: Fn(I, S) -> ParseResult<I, O2, S> + Clone,
{
    move |input: I, state: S| {
        let original_input = input.clone();
        let original_state = state.clone();
        let mut prev_input: I;
        let mut input = input;
        let mut state = state;
        let mut acc = Vec::new();

        loop {
            prev_input = input.clone();

            match end(input, state) {
                Ok((i, output, s)) => return Ok((i, (acc, output), s)),
                Err((i, _, s)) => match parser(i, s) {
                    Ok((i, output, s)) => {
                        if i == prev_input {
                            let err_msg = String::from("many_till0 successfully applied its parser, but failed to consume any input.");

                            return Err((
                                original_input,
                                ParseError(InfiniteLoop(err_msg), None),
                                original_state,
                            ));
                        } else {
                            acc.push(output);
                            input = i;
                            state = s;
                        }
                    }
                    Err((_, reason, _)) => return Err((original_input, reason, original_state)),
                },
            }
        }
    }
}

/// Applies `parser` at least once and then tries to apply `end`, if `end` is unsuccessful then it continues to try to apply `parser` followed by `end` until `parser` fails or `end` succeeds. If this is successful, it returns a tuple with the list of the outputs of `parser` and the output of `end`.
///
/// # Errors
///
/// If `parser` is applied and returns a successful result, but does not consume any of the input, this will return an error in order to prevent an infinite loop.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::many_till1;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
/// use rparsec::error::{ParseError, ParseErrorKind::InputMismatch};
///
/// let parser = many_till1(character('a'), character('b'));
/// let successful_input = "aaaaaabb";
/// let failing_input1 = "bb";
/// let failing_input2 = "aaaaaacc";
/// let state = NullState {};
///
/// assert_eq!(parser(successful_input, state), Ok(("b", (vec!['a', 'a', 'a', 'a', 'a', 'a'], 'b'), state)));
/// assert_eq!(parser(failing_input1, state), Err(("bb", ParseError(InputMismatch(String::from("Expected character \'a\', found \'b\'.")), None), state)));
/// assert_eq!(parser(failing_input2, state), Err(("aaaaaacc", ParseError(InputMismatch(String::from("Expected character \'a\', found \'c\'.")), None), state)));
/// ```
pub fn many_till1<I, O1, O2, S, F1, F2>(
    parser: F1,
    end: F2,
) -> impl Fn(I, S) -> ParseResult<I, (Vec<O1>, O2), S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O1: Clone,
    O2: Clone,
    F1: Fn(I, S) -> ParseResult<I, O1, S> + Clone,
    F2: Fn(I, S) -> ParseResult<I, O2, S> + Clone,
{
    move |input: I, state: S| {
        let original_input = input.clone();
        let original_state = state.clone();

        pair(parser.clone(), many_till0(parser.clone(), end.clone()))(input, state)
            .map(|(input, (initial, (acc, terminator)), state)| {
                let mut new_acc = vec![initial];

                new_acc.extend_from_slice(acc.as_slice());

                (input, (new_acc, terminator), state)
            })
            .map_err(|(_, reason, _)| (original_input, reason, original_state))
    }
}

/// Parses zero or more occurrences of `parser` separated by `separator` and returns a Vec containing the outputs of the successful `parser` matches.
///
/// # Errors
///
/// If `parser` is applied and returns a successful result, but does not consume any of the input, this will return an error in order to prevent an infinite loop.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::sep_by0;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
///
/// let parser = sep_by0(character('a'), character(','));
/// let input1 = "aa";
/// let input2 = "bb";
/// let input3 = "a,a,ab";
/// let state = NullState {};
///
/// assert_eq!(parser(input1, state), Ok(("a", vec!['a'], state)));
/// assert_eq!(parser(input2, state), Ok(("bb", vec![], state)));
/// assert_eq!(parser(input3, state), Ok(("b", vec!['a', 'a', 'a'], state)));
/// ```
pub fn sep_by0<I, O1, O2, S, F1, F2>(
    parser: F1,
    separator: F2,
) -> impl Fn(I, S) -> ParseResult<I, Vec<O1>, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O1: Clone,
    O2: Clone,
    F1: Fn(I, S) -> ParseResult<I, O1, S> + Clone,
    F2: Fn(I, S) -> ParseResult<I, O2, S> + Clone,
{
    move |input: I, state: S| {
        let original_input = input.clone();
        let original_state = state.clone();

        pair(
            parser.clone(),
            many0(preceded(separator.clone(), parser.clone())),
        )(input, state)
        .map(|(input, (initial, acc), state)| {
            let mut new_acc = vec![initial];

            new_acc.extend_from_slice(acc.as_slice());

            (input, new_acc, state)
        })
        .or_else(|_| Ok((original_input, vec![], original_state)))
    }
}

/// Parses one or more occurrences of `parser` separated by `separator` and returns a Vec<O1> containing the outputs of the successful `parser` matches.
///
/// # Errors
///
/// If `parser` is applied and returns a successful result, but does not consume any of the input, this will return an error in order to prevent an infinite loop.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::sep_by1;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
/// use rparsec::error::{ParseError, ParseErrorKind::InputMismatch};
///
/// let parser = sep_by1(character('a'), character(','));
/// let successful_input1 = "ab";
/// let successful_input2 = "a,ab";
/// let successful_input3 = "a,b";
/// let failing_input = "bb";
/// let state = NullState {};
///
/// assert_eq!(parser(successful_input1, state), Ok(("b", vec!['a'], state)));
/// assert_eq!(parser(successful_input2, state), Ok(("b", vec!['a', 'a'], state)));
/// assert_eq!(parser(successful_input3, state), Ok((",b", vec!['a'], state)));
/// assert_eq!(parser(failing_input, state), Err(("bb", ParseError(InputMismatch(String::from("Expected character \'a\', found \'b\'.")), None), state)));
/// ```
pub fn sep_by1<I, O1, O2, S, F1, F2>(
    parser: F1,
    separator: F2,
) -> impl Fn(I, S) -> ParseResult<I, Vec<O1>, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O1: Clone,
    O2: Clone,
    F1: Fn(I, S) -> ParseResult<I, O1, S> + Clone,
    F2: Fn(I, S) -> ParseResult<I, O2, S> + Clone,
{
    move |input: I, state: S| {
        let original_input = input.clone();
        let original_state = state.clone();

        pair(
            parser.clone(),
            many0(preceded(separator.clone(), parser.clone())),
        )(input, state)
        .map(|(input, (initial, acc), state)| {
            let mut new_acc = vec![initial];

            new_acc.extend_from_slice(acc.as_slice());

            (input, new_acc, state)
        })
        .map_err(|(_, reason, _)| (original_input, reason, original_state))
    }
}

/// Parses zero or more occurrences of `parser` separated and ended by `separator` and returns a Vec containing the outputs of the successful `parser` matches.
///
/// # Errors
///
/// If `parser` is applied and returns a successful result, but does not consume any of the input, this will return an error in order to prevent an infinite loop.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::end_by0;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
///
/// let parser = end_by0(character('a'), character(','));
/// let input1 = "a,a,b";
/// let input2 = "bb";
/// let input3 = "ab";
/// let input4 = "a,ab";
/// let state = NullState {};
///
/// assert_eq!(parser(input1, state), Ok(("b", vec!['a', 'a'], state)));
/// assert_eq!(parser(input2, state), Ok(("bb", vec![], state)));
/// assert_eq!(parser(input3, state), Ok(("ab", vec![], state)));
/// assert_eq!(parser(input4, state), Ok(("ab", vec!['a'], state)));
/// ```
pub fn end_by0<I, O1, O2, S, F1, F2>(
    parser: F1,
    separator: F2,
) -> impl Fn(I, S) -> ParseResult<I, Vec<O1>, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O1: Clone,
    O2: Clone,
    F1: Fn(I, S) -> ParseResult<I, O1, S> + Clone,
    F2: Fn(I, S) -> ParseResult<I, O2, S> + Clone,
{
    many0(terminated(parser, separator))
}

/// Parses one or more occurrences of `parser` separated and ended by `separator` and returns a Vec<O1> containing the outputs of the successful `parser` matches.
///
/// # Errors
///
/// If `parser` is applied and returns a successful result, but does not consume any of the input, this will return an error in order to prevent an infinite loop.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::end_by1;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
/// use rparsec::error::{ParseError, ParseErrorKind::InputMismatch};
///
/// let parser = end_by1(character('a'), character(','));
/// let input1 = "a,a,b";
/// let input2 = "a,ab";
/// let input3 = "ab";
/// let input4 = "b,ab";
/// let state = NullState {};
///
/// assert_eq!(parser(input1, state), Ok(("b", vec!['a', 'a'], state)));
/// assert_eq!(parser(input2, state), Ok(("ab", vec!['a'], state)));
/// assert_eq!(parser(input3, state), Err(("ab", ParseError(InputMismatch(String::from("Expected character \',\', found \'b\'.")), None), state)));
/// assert_eq!(parser(input4, state), Err(("b,ab", ParseError(InputMismatch(String::from("Expected character \'a\', found \'b\'.")), None), state)));
/// ```
pub fn end_by1<I, O1, O2, S, F1, F2>(
    parser: F1,
    separator: F2,
) -> impl Fn(I, S) -> ParseResult<I, Vec<O1>, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O1: Clone,
    O2: Clone,
    F1: Fn(I, S) -> ParseResult<I, O1, S> + Clone,
    F2: Fn(I, S) -> ParseResult<I, O2, S> + Clone,
{
    many1(terminated(parser, separator))
}

/// Parses zero or more occurrences of `parser` separated and optionally ended by `separator` and returns a Vec<O1> containing the outputs of the successful `parser` matches.
///
/// # Errors
///
/// If `parser` is applied and returns a successful result, but does not consume any of the input, this will return an error in order to prevent an infinite loop.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::sep_end_by0;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
///
/// let parser = sep_end_by0(character('a'), character(','));
/// let input1 = "a,a,b";
/// let input2 = "a,ab";
/// let input3 = "ab";
/// let input4 = "b";
/// let state = NullState {};
///
/// assert_eq!(parser(input1, state), Ok(("b", vec!['a', 'a'], state)));
/// assert_eq!(parser(input2, state), Ok(("b", vec!['a', 'a'], state)));
/// assert_eq!(parser(input3, state), Ok(("b", vec!['a'], state)));
/// assert_eq!(parser(input4, state), Ok(("b", vec![], state)));
/// ```
pub fn sep_end_by0<I, O1, O2, S, F1, F2>(
    parser: F1,
    separator: F2,
) -> impl Fn(I, S) -> ParseResult<I, Vec<O1>, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O1: Clone,
    O2: Clone,
    F1: Fn(I, S) -> ParseResult<I, O1, S> + Clone,
    F2: Fn(I, S) -> ParseResult<I, O2, S> + Clone,
{
    terminated(sep_by0(parser, separator.clone()), option(separator))
}

/// Parses one or more occurrences of `parser` separated and optionally ended by `separator` and returns a Vec<O1> containing the outputs of the successful `parser` matches.
///
/// # Errors
///
/// If `parser` is applied and returns a successful result, but does not consume any of the input, this will return an error in order to prevent an infinite loop.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::combinator::sep_end_by1;
/// use rparsec::text::character;
/// use rparsec::state::NullState;
/// use rparsec::error::{ParseError, ParseErrorKind::InputMismatch};
///
/// let parser = sep_end_by1(character('a'), character(','));
/// let input1 = "a,a,b";
/// let input2 = "a,ab";
/// let input3 = "ab";
/// let input4 = "b";
/// let state = NullState {};
///
/// assert_eq!(parser(input1, state), Ok(("b", vec!['a', 'a'], state)));
/// assert_eq!(parser(input2, state), Ok(("b", vec!['a', 'a'], state)));
/// assert_eq!(parser(input3, state), Ok(("b", vec!['a'], state)));
/// assert_eq!(parser(input4, state), Err(("b", ParseError(InputMismatch(String::from("Expected character \'a\', found \'b\'.")), None), state)));
/// ```
pub fn sep_end_by1<I, O1, O2, S, F1, F2>(
    parser: F1,
    separator: F2,
) -> impl Fn(I, S) -> ParseResult<I, Vec<O1>, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    O1: Clone,
    O2: Clone,
    F1: Fn(I, S) -> ParseResult<I, O1, S> + Clone,
    F2: Fn(I, S) -> ParseResult<I, O2, S> + Clone,
{
    terminated(sep_by1(parser, separator.clone()), option(separator))
}
