use std::fmt::Debug;

use super::error::{
    ParseError,
    ParseErrorKind::{InputMismatch, UnexpectedEndOfInput, Utf8ParseError},
};

use super::{
    input::{AsChar, ParserInput, ParserItem, ToChars},
    state::ParserState,
    ParseResult,
};

/// Attempts to consume a single whitespace character. Returns that character if successful.
///
/// # Examples
///
/// ```
/// extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::text::whitespace;
///
/// let state = NullState {};
///
/// let space_result = whitespace(" foo", state);
/// let tab_result = whitespace("\tfoo", state);
/// let newline_result = whitespace("\nfoo", state);
///
/// assert!(space_result.is_ok());
/// assert!(tab_result.is_ok());
/// assert!(newline_result.is_ok());
///
/// let space_result = space_result.unwrap();
/// let tab_result = tab_result.unwrap();
/// let newline_result = newline_result.unwrap();
///
/// assert_eq!(space_result.0, "foo");
/// assert_eq!(tab_result.0, "foo");
/// assert_eq!(newline_result.0, "foo");
///
/// assert_eq!(space_result.1, ' ');
/// assert_eq!(tab_result.1, '\t');
/// assert_eq!(newline_result.1, '\n');
/// ```
pub fn whitespace<I, S>(input: I, state: S) -> ParseResult<I, I::Item, S>
where
    I: ParserInput,
    S: ParserState,
{
    match input.cons() {
        Some((input_item, remaining)) => {
            if input_item.is_whitespace() {
                Ok((
                    remaining,
                    input_item.clone(),
                    state.update(input.next_as_slice()),
                ))
            } else {
                let err_msg = format!(
                    "Expected whitespace character, found '{}'.",
                    input_item.as_char().escape_default()
                );

                Err((input, ParseError(InputMismatch(err_msg), None), state))
            }
        }
        None => {
            let err_msg = String::from("Expected whitespace character, reached end of input.");

            Err((
                input,
                ParseError(UnexpectedEndOfInput(err_msg), None),
                state,
            ))
        }
    }
}

/// Attempts to consume a single newline character (\n). Returns that character if successful.
///
/// # Examples
///
/// ```
/// extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::text::newline;
///
/// let state = NullState {};
///
/// let result = newline("\nfoo", state);
///
/// assert!(result.is_ok());
///
/// let result = result.unwrap();
///
/// assert_eq!(result.0, "foo");
/// assert_eq!(result.1, '\n');
/// ```
pub fn newline<I, S>(input: I, state: S) -> ParseResult<I, I::Item, S>
where
    I: ParserInput,
    S: ParserState,
{
    match input.cons() {
        Some((input_item, remaining)) => {
            if input_item.as_char() == '\n' {
                Ok((
                    remaining,
                    input_item.clone(),
                    state.update(input.next_as_slice()),
                ))
            } else {
                let err_msg = format!(
                    "Expected newline character, found '{}'.",
                    input_item.as_char().escape_default()
                );

                Err((input, ParseError(InputMismatch(err_msg), None), state))
            }
        }
        None => {
            let err_msg = String::from("Expected newline character, reached end of input.");

            Err((
                input,
                ParseError(UnexpectedEndOfInput(err_msg), None),
                state,
            ))
        }
    }
}

/// Attempts to consume a single uppercase character. Returns that character if successful.
///
/// # Examples
///
/// ```
/// extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::text::uppercase;
///
/// let state = NullState {};
///
/// let result = uppercase("Foo", state);
///
/// assert!(result.is_ok());
///
/// let result = result.unwrap();
///
/// assert_eq!(result.0, "oo");
/// assert_eq!(result.1, 'F');
/// ```
pub fn uppercase<I, S>(input: I, state: S) -> ParseResult<I, I::Item, S>
where
    I: ParserInput,
    S: ParserState,
{
    match input.cons() {
        Some((input_item, remaining)) => {
            if input_item.is_uppercase() {
                Ok((
                    remaining,
                    input_item.clone(),
                    state.update(input.next_as_slice()),
                ))
            } else {
                let err_msg = format!(
                    "Expected uppercase character, found '{}'.",
                    input_item.as_char().escape_default()
                );

                Err((input, ParseError(InputMismatch(err_msg), None), state))
            }
        }
        None => {
            let err_msg = String::from("Expected uppercase character, reached end of input.");

            Err((
                input,
                ParseError(UnexpectedEndOfInput(err_msg), None),
                state,
            ))
        }
    }
}

/// Attempts to consume a single lowercase character. Returns that character if successful.
///
/// # Examples
///
/// ```
/// extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::text::lowercase;
///
/// let state = NullState {};
///
/// let result = lowercase("foo", state);
///
/// assert!(result.is_ok());
///
/// let result = result.unwrap();
///
/// assert_eq!(result.0, "oo");
/// assert_eq!(result.1, 'f');
/// ```
pub fn lowercase<I, S>(input: I, state: S) -> ParseResult<I, I::Item, S>
where
    I: ParserInput,
    S: ParserState,
{
    match input.cons() {
        Some((input_item, remaining)) => {
            if input_item.is_lowercase() {
                Ok((
                    remaining,
                    input_item.clone(),
                    state.update(input.next_as_slice()),
                ))
            } else {
                let err_msg = format!(
                    "Expected lowercase character, found '{}'.",
                    input_item.as_char().escape_default()
                );

                Err((input, ParseError(InputMismatch(err_msg), None), state))
            }
        }
        None => {
            let err_msg = String::from("Expected lowercase character, reached end of input.");

            Err((
                input,
                ParseError(UnexpectedEndOfInput(err_msg), None),
                state,
            ))
        }
    }
}

/// Attempts to consume a single alphabetic character. Returns that character if successful.
///
/// # Examples
///
/// ```
/// extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::text::alphabetic;
///
/// let state = NullState {};
///
/// let result = alphabetic("foo", state);
///
/// assert!(result.is_ok());
///
/// let result = result.unwrap();
///
/// assert_eq!(result.0, "oo");
/// assert_eq!(result.1, 'f');
/// ```
pub fn alphabetic<I, S>(input: I, state: S) -> ParseResult<I, I::Item, S>
where
    I: ParserInput,
    S: ParserState,
{
    match input.cons() {
        Some((input_item, remaining)) => {
            if input_item.is_alphabetic() {
                Ok((
                    remaining,
                    input_item.clone(),
                    state.update(input.next_as_slice()),
                ))
            } else {
                let err_msg = format!(
                    "Expected alphabetic character, found '{}'.",
                    input_item.as_char().escape_default()
                );

                Err((input, ParseError(InputMismatch(err_msg), None), state))
            }
        }
        None => {
            let err_msg = String::from("Expected alphabetic character, reached end of input.");

            Err((
                input,
                ParseError(UnexpectedEndOfInput(err_msg), None),
                state,
            ))
        }
    }
}

/// Attempts to consume a single numeric character. Returns that character if successful.
///
/// # Examples
///
/// ```
/// extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::text::numeric;
///
/// let state = NullState {};
///
/// let result = numeric("1foo", state);
///
/// assert!(result.is_ok());
///
/// let result = result.unwrap();
///
/// assert_eq!(result.0, "foo");
/// assert_eq!(result.1, '1');
/// ```
pub fn numeric<I, S>(input: I, state: S) -> ParseResult<I, I::Item, S>
where
    I: ParserInput,
    S: ParserState,
{
    match input.cons() {
        Some((input_item, remaining)) => {
            if input_item.is_numeric() {
                Ok((
                    remaining,
                    input_item.clone(),
                    state.update(input.next_as_slice()),
                ))
            } else {
                let err_msg = format!(
                    "Expected numeric character, found '{}'.",
                    input_item.as_char().escape_default()
                );

                Err((input, ParseError(InputMismatch(err_msg), None), state))
            }
        }
        None => {
            let err_msg = String::from("Expected numeric character, reached end of input.");

            Err((
                input,
                ParseError(UnexpectedEndOfInput(err_msg), None),
                state,
            ))
        }
    }
}

/// Attempts to consume a single alphanumeric character. Returns that character if successful.
///
/// # Examples
///
/// ```
/// extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::text::alphanumeric;
///
/// let state = NullState {};
///
/// let alphabetic_result = alphanumeric("foo", state);
/// let numeric_result = alphanumeric("1foo", state);
///
/// assert!(alphabetic_result.is_ok());
/// assert!(numeric_result.is_ok());
///
/// let alphabetic_result = alphabetic_result.unwrap();
/// let numeric_result = numeric_result.unwrap();
///
/// assert_eq!(alphabetic_result.0, "oo");
/// assert_eq!(numeric_result.0, "foo");
///
/// assert_eq!(alphabetic_result.1, 'f');
/// assert_eq!(numeric_result.1, '1');
/// ```
pub fn alphanumeric<I, S>(input: I, state: S) -> ParseResult<I, I::Item, S>
where
    I: ParserInput,
    S: ParserState,
{
    match input.cons() {
        Some((input_item, remaining)) => {
            if input_item.is_alphanumeric() {
                Ok((
                    remaining,
                    input_item.clone(),
                    state.update(input.next_as_slice()),
                ))
            } else {
                let err_msg = format!(
                    "Expected alphanumeric character, found '{}'.",
                    input_item.as_char().escape_default()
                );

                Err((input, ParseError(InputMismatch(err_msg), None), state))
            }
        }
        None => {
            let err_msg = String::from("Expected alphanumeric character, reached end of input.");

            Err((
                input,
                ParseError(UnexpectedEndOfInput(err_msg), None),
                state,
            ))
        }
    }
}

/// Attempts to consume a single instance of a specific character. Returns that character if successful.
///
/// # Examples
///
/// ```
/// extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::text::character;
///
/// let state = NullState {};
///
/// let result = character('f')("foo", state);
///
/// assert!(result.is_ok());
///
/// let result = result.unwrap();
///
/// assert_eq!(result.0, "oo");
/// assert_eq!(result.1, 'f');
/// ```
pub fn character<I, S>(ch: char) -> impl Fn(I, S) -> ParseResult<I, I::Item, S> + Clone
where
    I: ParserInput,
    S: ParserState,
{
    move |input: I, state: S| match input.cons() {
        Some((input_item, remaining)) => {
            if input_item.as_char() == ch {
                Ok((
                    remaining,
                    input_item.clone(),
                    state.update(input.next_as_slice()),
                ))
            } else {
                let err_msg = format!(
                    "Expected character '{}', found '{}'.",
                    ch,
                    input_item.as_char().escape_default()
                );

                Err((input, ParseError(InputMismatch(err_msg), None), state))
            }
        }
        None => {
            let err_msg = format!("Expected character '{}', reached end of input.", ch);

            Err((
                input,
                ParseError(UnexpectedEndOfInput(err_msg), None),
                state,
            ))
        }
    }
}

/// Accepts a &str and attempts to consume from the input until the full string has been matched or a comparison fails. Returns that string if successful.
///
/// This parser breaks the given string into a sequence of `char`s for comparison. If it contains any multi-byte UTF-8 code points and the input is a `&[u8]` then a match will not be possible.
///
/// # Examples
///
/// ```
/// extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::text::string;
///
/// let state = NullState {};
///
/// let result = string("foo")("foo1", state);
///
/// assert!(result.is_ok());
///
/// let result = result.unwrap();
///
/// assert_eq!(result.0, "1");
/// assert_eq!(result.1, "foo");
/// ```
pub fn string<I, S>(
    matching_string: &'static str,
) -> impl Fn(I, S) -> ParseResult<I, &'static str, S> + Clone
where
    I: ParserInput,
    S: ParserState,
{
    move |input: I, state: S| {
        let mut remaining = input.clone();

        for (i, char_to_match) in matching_string.char_indices() {
            match remaining.cons() {
                Some((input_item, r)) => {
                    if input_item.as_char() == char_to_match {
                        remaining = r;
                    } else {
                        let err_msg = format!("Attempting to match string \"{}\", expected '{}' at position {} and found '{}'.", matching_string, char_to_match.escape_default(), i, input_item.as_char().escape_default());

                        return Err((input, ParseError(InputMismatch(err_msg), None), state));
                    }
                }
                None => {
                    let err_msg = format!("Attempting to match string \"{}\", expected '{}' at position {} and reached end of input.", matching_string, char_to_match.escape_default(), i);

                    return Err((
                        input,
                        ParseError(UnexpectedEndOfInput(err_msg), None),
                        state,
                    ));
                }
            }
        }

        Ok((remaining, matching_string, state.update(matching_string)))
    }
}

/// Attempts to consume any single character for &str inputs or a single byte for &[u8] inputs. Returns that char/byte if successful.
///
/// # Examples
///
/// ```
/// extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::text::any_char;
///
/// let state = NullState {};
///
/// let result = any_char("foo", state);
///
/// assert!(result.is_ok());
///
/// let result = result.unwrap();
///
/// assert_eq!(result.0, "oo");
/// assert_eq!(result.1, 'f');
/// ```
pub fn any_char<I, S>(input: I, state: S) -> ParseResult<I, I::Item, S>
where
    I: ParserInput,
    S: ParserState,
{
    match input.cons() {
        Some((c, remaining)) => Ok((remaining, c.clone(), state.update(input.next_as_slice()))),
        None => {
            let err_msg = String::from("Expected any single character, reached end of input.");

            Err((
                input,
                ParseError(UnexpectedEndOfInput(err_msg), None),
                state,
            ))
        }
    }
}

/// Accepts an input that implements `rparsec::input::ToChars` and attempts to consume a single character that matches any of the characters in the input. Returns that character if successful.
///
/// If the input cannot be parsed as a valid utf8 byte sequence then this parser will fail.
///
/// Keep in mind that `char`s are not the same as glyphs and may not match human intuition for what constitutes a character.
///
/// # Examples
///
/// ```
/// extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::text::any_of;
///
/// let state = NullState {};
///
/// let a_result = any_of("abc")("aaa", state);
/// let b_result = any_of("abc")("bbb", state);
/// let c_result = any_of("abc")("ccc", state);
/// let d_result = any_of("abc")("ddd", state);
///
/// assert!(a_result.is_ok());
/// assert!(b_result.is_ok());
/// assert!(c_result.is_ok());
/// assert!(d_result.is_err());
///
/// let a_result = a_result.unwrap();
/// let b_result = b_result.unwrap();
/// let c_result = c_result.unwrap();
///
/// assert_eq!(a_result.0, "aa");
/// assert_eq!(a_result.1, 'a');
/// assert_eq!(b_result.0, "bb");
/// assert_eq!(b_result.1, 'b');
/// assert_eq!(c_result.0, "cc");
/// assert_eq!(c_result.1, 'c');
/// ```
pub fn any_of<I, S, T>(matching_any_of: T) -> impl Fn(I, S) -> ParseResult<I, I::Item, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    T: ToChars + Debug + Clone,
{
    move |input: I, state: S| match matching_any_of.chars() {
        Ok(mut matching_chars) => match input.cons() {
            Some((input_item, remaining)) => {
                if matching_chars.any(|c| input_item.as_char() == c) {
                    Ok((
                        remaining,
                        input_item.clone(),
                        state.update(input.next_as_slice()),
                    ))
                } else {
                    let err_msg = format!(
                        "Attempted to match any of \"{:?}\" and found '{}'.",
                        matching_any_of,
                        input_item.as_char().escape_default()
                    );

                    Err((input, ParseError(InputMismatch(err_msg), None), state))
                }
            }
            None => {
                let err_msg = format!(
                    "Attempted to match any of \"{:?}\" and reached end of input.",
                    matching_any_of
                );

                Err((
                    input,
                    ParseError(UnexpectedEndOfInput(err_msg), None),
                    state,
                ))
            }
        },
        Err(_) => {
            let err_msg = format!("Attempted to match any character in \"{:?}\", but it could not be parsed as valid UTF-8.", matching_any_of);

            Err((input, ParseError(Utf8ParseError(err_msg), None), state))
        }
    }
}

/// Accepts an input that implements `rparsec::input::ToChars` and attempts to consume a single character that does not match any of the characters in the input. Returns that character if successful.
///
/// If the input cannot be parsed as a valid utf8 byte sequence then this parser will fail.
///
/// Keep in mind that `char`s are not the same as glyphs and may not match human intuition for what constitutes a character.
///
/// # Examples
///
/// ```
/// extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::text::none_of;
///
/// let state = NullState {};
///
/// let d_result = none_of("abc")("ddd", state);
/// let e_result = none_of("abc")("eee", state);
/// let f_result = none_of("abc")("fff", state);
/// let a_result = none_of("abc")("aaa", state);
///
/// assert!(d_result.is_ok());
/// assert!(e_result.is_ok());
/// assert!(f_result.is_ok());
/// assert!(a_result.is_err());
///
/// let d_result = d_result.unwrap();
/// let e_result = e_result.unwrap();
/// let f_result = f_result.unwrap();
///
/// assert_eq!(d_result.0, "dd");
/// assert_eq!(d_result.1, 'd');
/// assert_eq!(e_result.0, "ee");
/// assert_eq!(e_result.1, 'e');
/// assert_eq!(f_result.0, "ff");
/// assert_eq!(f_result.1, 'f');
/// ```
pub fn none_of<I, S, T>(matching_none_of: T) -> impl Fn(I, S) -> ParseResult<I, I::Item, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    T: ToChars + Debug + Clone,
{
    move |input: I, state: S| match matching_none_of.chars() {
        Ok(mut matching_chars) => {
            match input.cons() {
                Some((input_item, remaining)) => {
                    if matching_chars.any(|c| input_item.as_char() == c) {
                        let err_msg = format!(
                            "Attempted to match any character not in \"{:?}\" and found '{}'.",
                            matching_none_of,
                            input_item.as_char().escape_default()
                        );

                        Err((input, ParseError(InputMismatch(err_msg), None), state))
                    } else {
                        Ok((
                            remaining,
                            input_item.clone(),
                            state.update(input.next_as_slice()),
                        ))
                    }
                }
                None => {
                    let err_msg = format!("Attempted to match any character not in \"{:?}\" and reached end of input.", matching_none_of);

                    Err((
                        input,
                        ParseError(UnexpectedEndOfInput(err_msg), None),
                        state,
                    ))
                }
            }
        }
        Err(_) => {
            let err_msg = format!("Attempted to match any character not in \"{:?}\", but it could not be parsed as valid UTF-8.", matching_none_of);

            Err((input, ParseError(Utf8ParseError(err_msg), None), state))
        }
    }
}

/// Accepts a `Fn(ParserInput::Item) -> bool` and generates a parser that attempts to consume a character for which that function returns true.
///
/// # Examples
///
/// ```
/// extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::text::satisfies;
///
/// let state = NullState {};
///
/// let result = satisfies(|c| c == 'a')("abc", state);
///
/// assert!(result.is_ok());
///
/// let result = result.unwrap();
///
/// assert_eq!(result.0, "bc");
/// assert_eq!(result.1, 'a');
/// ```
pub fn satisfies<I, S, F>(f: F) -> impl Fn(I, S) -> ParseResult<I, I::Item, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    I::Item: ParserItem,
    F: Fn(I::Item) -> bool + Clone,
{
    move |input: I, state: S| match input.cons() {
        Some((input_item, remaining)) => {
            if f(input_item.clone()) {
                Ok((
                    remaining,
                    input_item.clone(),
                    state.update(input.next_as_slice()),
                ))
            } else {
                let err_msg = format!(
                    "Attempted to match a character satisfying a given predicate and found '{}'.",
                    input_item.as_char().escape_default()
                );

                Err((input, ParseError(InputMismatch(err_msg), None), state))
            }
        }
        None => {
            let err_msg = String::from("Attempted to match a character satisfying a given predicate and reached end of input.");

            Err((
                input,
                ParseError(UnexpectedEndOfInput(err_msg), None),
                state,
            ))
        }
    }
}

/// Attempts to match any byte sequence defined as a line terminator by the Unicode standard:
///
/// ``` ignore
/// LF:    Line Feed, U+000A
/// VT:    Vertical Tab, U+000B
/// FF:    Form Feed, U+000C
/// CR:    Carriage Return, U+000D
/// CR+LF: CR (U+000D) followed by LF (U+000A)
/// NEL:   Next Line, U+0085
/// LS:    Line Separator, U+2028
/// PS:    Paragraph Separator, U+2029
/// ```
///
/// This parser only matches explicit line terminators, if it reaches the end of the input it will fail.
///
/// # Examples
///
pub fn eol<I, S>(input: I, state: S) -> ParseResult<I, I, S>
where
    I: ParserInput,
    S: ParserState,
{
    match input.match_eol() {
        Some((terminator, remaining)) => {
            Ok((remaining, terminator.clone(), state.update(terminator)))
        }
        None if input.is_empty() => {
            let err_msg =
                String::from("Attempted to match a line terminator and reached end of input.");

            Err((
                input,
                ParseError(UnexpectedEndOfInput(err_msg), None),
                state,
            ))
        }
        None => {
            let (next, _) = input.cons().unwrap();
            let err_msg = format!(
                "Attempted to match a line terminator, but found '{}'.",
                next.as_char().escape_default()
            );

            Err((input, ParseError(InputMismatch(err_msg), None), state))
        }
    }
}
