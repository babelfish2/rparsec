use std::fmt::Debug;
use std::hash::Hash;

use super::input::ParserInput;

use unic::segment::Graphemes;

/// If you have no need for a state object, this will let you pass around one which does nothing.
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct NullState {}

impl ParserState for NullState {
    fn update<I: AsRef<[u8]>>(self, consumed_input: I) -> Self {
        self
    }
}

impl Default for NullState {
    fn default() -> Self {
        Self {}
    }
}

/// This state object tracks the position within the input as it is consumed by the parser.
/// It tracks consumed input and lazily evaluates the position when it is requested.
/// Because position info could be requested in the middle of a multi-byte UTF-8 sequence,
/// and because there is no guarantee that &[u8] inputs will be valid UTF-8, it uses
/// `String::from_utf8_lossy` to parse the input. A `byte_offset` method is also provided
/// in case the exact position within the original input is needed.
///
/// The line count honors any sequence defined as a line terminator by the Unicode standard,
/// not just \n and \r\n.
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Utf8InputPosition {
    line: usize,
    column: usize,
    byte_offset: usize,
    current_line: String,
    buffer: Vec<u8>,
}

impl Utf8InputPosition {
    pub fn new(capacity: usize) -> Self {
        Utf8InputPosition {
            line: 1,
            column: 0,
            byte_offset: 0,
            current_line: String::new(),
            buffer: Vec::with_capacity(capacity),
        }
    }

    /// ```
    /// extern crate rparsec;
    /// use rparsec::state::Utf8InputPosition;
    /// use rparsec::state::ParserState;
    ///
    /// let input = "abc\ndef\u{000B}ghi\u{000C}jkl\rmno\r\npqr\u{0085}stu\u{2028}vwx\u{2029}yz";
    /// let mut state = Utf8InputPosition::new(input.as_bytes().len()).update(input);
    ///
    /// assert_eq!(state.line(), 9);
    /// ```
    pub fn line(&mut self) -> usize {
        self.update_position();

        self.line
    }

    /// ```
    /// extern crate rparsec;
    /// use rparsec::state::Utf8InputPosition;
    /// use rparsec::state::ParserState;
    ///
    /// let input = "FrançaisРусскцй日本語Ελλενικα";
    /// let mut state = Utf8InputPosition::new(input.as_bytes().len()).update(input);
    ///
    /// assert_eq!(state.column(), 26);
    /// ```
    pub fn column(&mut self) -> usize {
        self.update_position();

        self.column
    }

    /// ```
    /// extern crate rparsec;
    /// use rparsec::state::Utf8InputPosition;
    /// use rparsec::state::ParserState;
    ///
    /// let input = "Français\u{0085}Русскцй\u{2028}日本語\u{2029}Ελλενικα";
    /// let mut state = Utf8InputPosition::new(input.as_bytes().len()).update(input);
    ///
    /// assert_eq!(state.position(), (4, 8));
    /// ```
    pub fn position(&mut self) -> (usize, usize) {
        self.update_position();

        (self.line, self.column)
    }

    /// ```
    /// extern crate rparsec;
    /// use rparsec::state::Utf8InputPosition;
    /// use rparsec::state::ParserState;
    ///
    /// let input = "Français\u{0085}Русскцй\u{2028}日本語\u{2029}Ελλενικα";
    /// let mut state = Utf8InputPosition::new(input.as_bytes().len()).update(input);
    ///
    /// assert_eq!(state.current_line(), "Ελλενικα");
    /// ```
    pub fn current_line(&mut self) -> &str {
        self.update_position();

        &self.current_line
    }

    /// ```
    /// extern crate rparsec;
    /// use rparsec::state::Utf8InputPosition;
    /// use rparsec::state::ParserState;
    ///
    /// let input = "Français\u{0085}Русскцй\u{2028}日本語\u{2029}Ελλενικα";
    /// let mut state = Utf8InputPosition::new(input.as_bytes().len()).update(input);
    ///
    /// assert_eq!(state.byte_offset(), input.as_bytes().len() - 1);
    /// ```
    pub fn byte_offset(&mut self) -> usize {
        self.update_position();

        self.byte_offset
    }

    fn update_position(&mut self) {
        self.byte_offset += self.buffer.len().saturating_sub(1);

        for grapheme in Graphemes::new(&String::from_utf8_lossy(self.buffer.as_slice())) {
            match grapheme.match_eol() {
                Some(_) => {
                    self.line += 1;
                    self.column = 0;

                    self.current_line.clear();
                }
                None => {
                    self.column += 1;

                    self.current_line.push_str(grapheme);
                }
            }
        }

        self.buffer.clear();
    }
}

impl ParserState for Utf8InputPosition {
    fn update<I: AsRef<[u8]>>(mut self, consumed_input: I) -> Self {
        for b in consumed_input.as_ref() {
            self.buffer.push(*b);
        }

        self
    }
}

impl Default for Utf8InputPosition {
    fn default() -> Self {
        Self::new(10)
    }
}

pub trait ParserState: Clone + Send + Sync + PartialEq + Eq + Hash + Debug + Default {
    fn update<I: AsRef<[u8]>>(self, consumed_input: I) -> Self;
}
