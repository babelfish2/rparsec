use std::fmt::{Debug, Display};
use std::hash::Hash;
use std::str::{Chars, Utf8Error};

const LF: char = '\n';
const VT: char = '\u{000B}';
const FF: char = '\u{000C}';
const CR: char = '\r';
const NEL: char = '\u{0085}';
const LS: char = '\u{2028}';
const PS: char = '\u{2029}';

pub trait AsChar {
    fn as_char(&self) -> char;
}

impl AsChar for &u8 {
    fn as_char(&self) -> char {
        **self as char
    }
}

impl AsChar for char {
    fn as_char(&self) -> char {
        *self
    }
}

pub trait ToChars {
    fn chars(&self) -> Result<Chars, Utf8Error>;
}

impl ToChars for &str {
    fn chars(&self) -> Result<Chars, Utf8Error> {
        Ok(str::chars(self))
    }
}

impl ToChars for &[u8] {
    fn chars(&self) -> Result<Chars, Utf8Error> {
        Ok(std::str::from_utf8(self)?.chars())
    }
}

pub trait ParserItem:
    AsChar + Clone + Debug + Display + Eq + Hash + Ord + PartialEq + PartialOrd + Send + Sync
{
    fn is_whitespace(&self) -> bool;
    fn is_alphabetic(&self) -> bool;
    fn is_uppercase(&self) -> bool;
    fn is_lowercase(&self) -> bool;
    fn is_numeric(&self) -> bool;
    fn is_alphanumeric(&self) -> bool;
}

impl ParserItem for &u8 {
    fn is_whitespace(&self) -> bool {
        char::is_whitespace(**self as char)
    }

    fn is_alphabetic(&self) -> bool {
        char::is_alphabetic(**self as char)
    }

    fn is_uppercase(&self) -> bool {
        char::is_uppercase(**self as char)
    }

    fn is_lowercase(&self) -> bool {
        char::is_lowercase(**self as char)
    }

    fn is_numeric(&self) -> bool {
        char::is_numeric(**self as char)
    }

    fn is_alphanumeric(&self) -> bool {
        char::is_alphanumeric(**self as char)
    }
}

impl ParserItem for char {
    fn is_whitespace(&self) -> bool {
        char::is_whitespace(*self)
    }

    fn is_alphabetic(&self) -> bool {
        char::is_alphabetic(*self)
    }

    fn is_uppercase(&self) -> bool {
        char::is_uppercase(*self)
    }

    fn is_lowercase(&self) -> bool {
        char::is_lowercase(*self)
    }

    fn is_numeric(&self) -> bool {
        char::is_numeric(*self)
    }

    fn is_alphanumeric(&self) -> bool {
        char::is_alphanumeric(*self)
    }
}

pub trait ParserInput:
    AsRef<[u8]> + Clone + Debug + Default + Eq + Hash + Ord + PartialEq + PartialOrd + Send + Sync
{
    type Item: ParserItem + Clone;

    fn cons(&self) -> Option<(Self::Item, Self)>;
    fn next_as_slice(&self) -> Self;
    fn iter<'a>(&'a self) -> Box<dyn Iterator<Item = Self::Item> + 'a>;

    /// Reads from the input as long as the provided function returns true for each input item. Splits the input at the first failure.
    ///
    /// ```
    /// # extern crate rparsec;
    /// use rparsec::input::ParserInput;
    /// use rparsec::input::ParserItem;
    ///
    /// let byte_input: &[u8] = &[0x0, 0x0, 0x1];
    ///
    /// assert_eq!("aaabc".take_while(|c| c == 'a'), ("aaa", "bc"));
    /// assert_eq!("abcAbc".take_while(|c| c.is_lowercase()), ("abc", "Abc"));
    /// assert_eq!(byte_input.take_while(|b| *b == 0x0), byte_input.split_at(2));
    /// ```
    fn take_while<F: FnMut(Self::Item) -> bool>(&self, f: F) -> (Self, Self);

    /// Attempts to consume any byte sequence defined as a line terminator by the Unicode standard:
    ///
    /// ``` ignore
    /// LF:    Line Feed, U+000A
    /// VT:    Vertical Tab, U+000B
    /// FF:    Form Feed, U+000C
    /// CR:    Carriage Return, U+000D
    /// CR+LF: CR (U+000D) followed by LF (U+000A)
    /// NEL:   Next Line, U+0085
    /// LS:    Line Separator, U+2028
    /// PS:    Paragraph Separator, U+2029
    /// ```
    ///
    /// It returns a tuple of that sequence as a slice of the original input and a slice with the remainder of the original input.
    ///
    /// ```
    /// # extern crate rparsec;
    /// use rparsec::input::ParserInput;
    ///
    /// assert_eq!("\nfoo".match_eol(), Some(("\n", "foo")));
    /// assert_eq!("\u{000B}foo".match_eol(), Some(("\u{000B}", "foo")));
    /// assert_eq!("\u{000C}foo".match_eol(), Some(("\u{000C}", "foo")));
    /// assert_eq!("\u{0085}foo".match_eol(), Some(("\u{0085}", "foo")));
    /// assert_eq!("\u{2028}foo".match_eol(), Some(("\u{2028}", "foo")));
    /// assert_eq!("\u{2029}foo".match_eol(), Some(("\u{2029}", "foo")));
    /// assert_eq!("\rfoo".match_eol(), Some(("\r", "foo")));
    /// assert_eq!("\r\nfoo".match_eol(), Some(("\r\n", "foo")));
    ///
    /// let lf_bytes: &[u8] = &[0x0a, 0x00];
    /// let vt_bytes: &[u8] = &[0x0b, 0x00];
    /// let ff_bytes: &[u8] = &[0x0c, 0x00];
    /// let cr_bytes: &[u8] = &[0x0d, 0x00];
    /// let crlf_bytes: &[u8] = &[0x0d, 0x0a, 0x00];
    /// let nel_bytes: &[u8] = &[0xc2, 0x85, 0x00];
    /// let ls_bytes: &[u8] = &[0xe2, 0x80, 0xa8, 0x00];
    /// let ps_bytes: &[u8] = &[0xe2, 0x80, 0xa9, 0x00];
    ///
    /// assert_eq!(lf_bytes.match_eol(), Some(lf_bytes.split_at(1)));
    /// assert_eq!(vt_bytes.match_eol(), Some(vt_bytes.split_at(1)));
    /// assert_eq!(ff_bytes.match_eol(), Some(ff_bytes.split_at(1)));
    /// assert_eq!(cr_bytes.match_eol(), Some(cr_bytes.split_at(1)));
    /// assert_eq!(crlf_bytes.match_eol(), Some(crlf_bytes.split_at(2)));
    /// assert_eq!(nel_bytes.match_eol(), Some(nel_bytes.split_at(2)));
    /// assert_eq!(ls_bytes.match_eol(), Some(ls_bytes.split_at(3)));
    /// assert_eq!(ps_bytes.match_eol(), Some(ps_bytes.split_at(3)));
    /// ```
    fn match_eol(&self) -> Option<(Self, Self)>;
    fn is_empty(&self) -> bool;
}

impl<'a> ParserInput for &'a [u8] {
    type Item = &'a u8;

    fn cons(&self) -> Option<(Self::Item, Self)> {
        match self.len() {
            0 => None,
            _ => Some((&self[0], &self[1..])),
        }
    }

    fn next_as_slice(&self) -> Self {
        &self[..1]
    }

    fn iter<'b>(&'b self) -> Box<dyn Iterator<Item = Self::Item> + 'b> {
        Box::new(<[u8]>::iter(self))
    }

    fn take_while<F: FnMut(Self::Item) -> bool>(&self, mut f: F) -> (Self, Self) {
        for (i, b) in self.iter().enumerate() {
            if !f(b) {
                return (&self[..i], &self[i..]);
            }
        }

        (self, &self[self.len()..])
    }

    fn match_eol(&self) -> Option<(Self, Self)> {
        let (first, remaining) = self.cons()?;

        // revisit this after advanced slice patterns make it into stable: https://github.com/rust-lang/rfcs/pull/2359
        match first {
            0x0a | 0x0b | 0x0c => Some(self.split_at(1)), // LF | VT | FF
            0x0d => {
                match remaining.cons() {
                    Some((0x0a, _)) => Some(self.split_at(2)), // CR+LF
                    _ => Some(self.split_at(1)),               // CR
                }
            }
            0xc2 => {
                match remaining.cons() {
                    Some((0x85, _)) => Some(self.split_at(2)), // NEL
                    _ => None,
                }
            }
            0xe2 => {
                match remaining.cons() {
                    Some((0x80, remaining)) => {
                        match remaining.cons() {
                            Some((0xa8, _)) | Some((0xa9, _)) => Some(self.split_at(3)), // LS | PS
                            _ => None,
                        }
                    }
                    _ => None,
                }
            }
            _ => None,
        }
    }

    fn is_empty(&self) -> bool {
        <[u8]>::is_empty(self)
    }
}

impl<'a> ParserInput for &'a str {
    type Item = char;

    fn cons(&self) -> Option<(Self::Item, Self)> {
        let mut chars = str::chars(self);

        chars.next().map(|first| (first, chars.as_str()))
    }

    fn next_as_slice(&self) -> Self {
        let next = self.cons().unwrap().0;

        self.split_at(next.len_utf8()).0
    }

    fn iter<'b>(&'b self) -> Box<dyn Iterator<Item = Self::Item> + 'b> {
        Box::new(str::chars(self))
    }

    fn take_while<F: FnMut(Self::Item) -> bool>(&self, mut f: F) -> (Self, Self) {
        for (i, b) in self.char_indices() {
            if !f(b) {
                return self.split_at(i);
            }
        }

        self.split_at(self.len())
    }

    fn match_eol(&self) -> Option<(Self, Self)> {
        let (first, remaining) = self.cons()?;

        match first {
            LF | VT | FF | NEL | LS | PS => {
                Some((self.get(..first.len_utf8()).unwrap(), remaining))
            }
            CR => match remaining.cons() {
                Some((second, remaining)) if second == LF => {
                    let len = first.len_utf8() + second.len_utf8();

                    Some((self.get(..len).unwrap(), remaining))
                }
                _ => Some((self.get(..first.len_utf8()).unwrap(), remaining)),
            },
            _ => None,
        }
    }

    fn is_empty(&self) -> bool {
        str::is_empty(self)
    }
}
