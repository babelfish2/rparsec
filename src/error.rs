use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct ParseError(pub ParseErrorKind, pub Option<Box<dyn Error>>);

impl ParseError {
    pub fn set_cause(mut self, cause: impl Error + 'static) -> Self {
        self.1 = Some(Box::new(cause));

        self
    }

    pub fn kind(&self) -> &ParseErrorKind {
        &self.0
    }

    pub fn root_cause(&self) -> Option<&(dyn Error + 'static)> {
        let mut cause: &(dyn Error + 'static) = self;

        loop {
            match cause.source() {
                Some(s) => {
                    cause = s;
                }
                None => return Some(cause),
            }
        }
    }
}

impl fmt::Display for ParseError {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        unimplemented!()
    }
}

impl PartialEq for ParseError {
    fn eq(&self, other: &Self) -> bool {
        self.kind() == other.kind()
    }
}

impl Error for ParseError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        self.1.as_ref().map(|x| &**x as _)
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum ParseErrorKind {
    Utf8ParseError(String),
    InputMismatch(String),
    UnexpectedEndOfInput(String),
    InfiniteLoop(String),
    InvalidParser(String),
    TransformFailure(String),
}

impl fmt::Display for ParseErrorKind {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            ParseErrorKind::Utf8ParseError(err_msg) => {
                write!(formatter, "{}", err_msg)
            }
            ParseErrorKind::InputMismatch(err_msg) => {
                write!(formatter, "{}", err_msg)
            }
            ParseErrorKind::UnexpectedEndOfInput(err_msg) => {
                write!(formatter, "{}", err_msg)
            }
            ParseErrorKind::InfiniteLoop(err_msg) => {
                write!(
                    formatter,
                    "{}. Halting parsing to prevent an infinite loop.",
                    err_msg
                )
            }
            ParseErrorKind::InvalidParser(err_msg) => {
                write!(formatter, "{}", err_msg)
            }
            ParseErrorKind::TransformFailure(err_msg) => {
                write!(formatter, "{}", err_msg)
            }
        }
    }
}
