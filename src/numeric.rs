//! This module is for parsing _text representations of numbers_. See the bytes module for parsing byte representations of numbers.

use lexical::{parse, FromLexical};

use crate::{
    error::{ParseError, ParseErrorKind::TransformFailure},
    input::{AsChar, ParserInput, ParserItem},
    state::ParserState,
    ParseResult,
};

/// Attempts to parse an integer value by consuming input until encountering something that isn't a text representation of a number or a leading sign marker. Returns that integer if successful.
///
/// IMPORTANT: This parser uses the FromLexical trait from the lexical crate to infer the integer type to return. See the examples below. Unless you are certain that a leading negative sign will not be present in the input, it is not a good idea to attempt to return an unsigned integer type. Due to the implementation details of the lexical crate, if a negative sign is present while trying to parse an unsigned integer it will always result in an integer overflow error.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::ParseResult;
/// use rparsec::state::NullState;
/// use rparsec::numeric::integer;
///
/// let state = NullState {};
///
/// let negative_result = integer::<_, _, i32>("-123abc", state);
/// let marked_positive_result = integer::<_, _, i32>("+123abc", state);
/// let unmarked_positive_result = integer::<_, _, i32>("123abc", state);
/// let u32_result = integer::<_, _, u32>("123abc", state);
/// let u16_result: ParseResult<_, u16, _> = integer("123abc", state);
///
/// assert_eq!(negative_result, Ok(("abc", -123i32, state)));
/// assert_eq!(marked_positive_result, Ok(("abc", 123i32, state)));
/// assert_eq!(unmarked_positive_result, Ok(("abc", 123i32, state)));
/// assert_eq!(u32_result, Ok(("abc", 123u32, state)));
/// assert_eq!(u16_result, Ok(("abc", 123u16, state)));
/// ```
pub fn integer<I, S, O: FromLexical>(input: I, state: S) -> ParseResult<I, O, S>
where
    I: ParserInput,
    S: ParserState,
{
    let original_input = input.clone();
    let original_state = state.clone();

    let mut parsing_first_item = true;

    let (consumed, remaining) = input.take_while(|i| {
        if parsing_first_item {
            parsing_first_item = false;

            match i.as_char() {
                '+' | '-' => true,
                i if i.is_numeric() => true,
                _ => false,
            }
        } else {
            i.is_numeric()
        }
    });

    parse::<O, _>(consumed.clone())
        .map(|n| (remaining, n, state.update(consumed)))
        .or_else(|e| {
            let err_msg = e.to_string();

            Err((
                original_input,
                ParseError(TransformFailure(err_msg), None),
                original_state,
            ))
        })
}
