use crate::{
    error::{ParseError, ParseErrorKind::InputMismatch},
    input::{ParserInput, ParserItem},
    state::ParserState,
    ParseResult,
};

/// Attempts to consume from the input for as long as the provided predicate returns true. Returns the consumed portion of the input as a slice of the original input.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::input::ParserItem;
/// use rparsec::misc::take_while0;
///
/// let state = NullState {};
///
/// let byte_input: &[u8] = &[0x20, 0x0];
///
/// let char_result = take_while0::<&str, _, _>(|c| c.is_whitespace())(" foo", state);
/// let byte_result = take_while0::<&[u8], _, _>(|c| c.is_whitespace())(byte_input, state);
/// let empty_result = take_while0::<&str, _, _>(|c| c.is_whitespace())("foo", state);
///
/// assert_eq!(char_result, Ok(("foo", " ", state)));
/// assert_eq!(byte_result, Ok((&byte_input[1..], &byte_input[..1], state)));
/// assert_eq!(empty_result, Ok(("foo", "", state)));
/// ```
pub fn take_while0<I, S, F>(f: F) -> impl Fn(I, S) -> ParseResult<I, I, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    I::Item: ParserItem,
    F: Fn(I::Item) -> bool + Clone,
{
    move |input: I, state: S| {
        let (consumed, remaining) = input.take_while(&f);

        Ok((remaining, consumed.clone(), state.update(consumed)))
    }
}

/// Attempts to consume from the input for as long as the provided predicate returns true. Fails if no input is consumed. Returns the consumed portion of the input as a slice of the original input.
///
/// # Examples
///
/// ```
/// # extern crate rparsec;
/// use rparsec::state::NullState;
/// use rparsec::error::{ParseError, ParseErrorKind::InputMismatch};
/// use rparsec::input::ParserItem;
/// use rparsec::misc::take_while1;
///
/// let state = NullState {};
///
/// let byte_input: &[u8] = &[0x20, 0x0];
///
/// let char_result = take_while1::<&str, _, _>(|c| c.is_whitespace())(" foo", state);
/// let byte_result = take_while1::<&[u8], _, _>(|c| c.is_whitespace())(byte_input, state);
/// let empty_result = take_while1::<&str, _, _>(|c| c.is_whitespace())("foo", state);
///
/// assert_eq!(char_result, Ok(("foo", " ", state)));
/// assert_eq!(byte_result, Ok((&byte_input[1..], &byte_input[..1], state)));
/// assert_eq!(empty_result, Err(("foo", ParseError(InputMismatch(String::from("take_while1 failed to consume any input.")), None), state)));
/// ```
pub fn take_while1<I, S, F>(f: F) -> impl Fn(I, S) -> ParseResult<I, I, S> + Clone
where
    I: ParserInput,
    S: ParserState,
    I::Item: ParserItem,
    F: Fn(I::Item) -> bool + Clone,
{
    move |input: I, state: S| {
        let (consumed, remaining) = input.take_while(&f);

        if remaining == input {
            let err_msg = String::from("take_while1 failed to consume any input.");

            Err((input, ParseError(InputMismatch(err_msg), None), state))
        } else {
            Ok((remaining, consumed.clone(), state.update(consumed)))
        }
    }
}
